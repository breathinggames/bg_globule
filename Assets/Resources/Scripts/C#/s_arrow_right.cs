﻿using UnityEngine;
using System.Collections;

public class s_arrow_right : MonoBehaviour {

	public GameObject camPointer;
	public GameObject mouseChangeDown;
	public GameObject mouseChangeUp;
	public GameObject mouseChangeLeft;
	public GameObject mouseChangeRight;
	
	
	void Start() {
	}
	
	void Update() {
	}

	void OnMouseDown() {
		if(s_cursor.v == true){
			mouseChangeRight.SetActive(true);
			s_input_time.arrow_right -= 1;
		}
		else if (s_cursor.v == false && s_cursor.b == true){
			mouseChangeRight.SetActive(false);
			mouseChangeUp.SetActive(true);
		}
		else if (s_cursor.v == false && s_cursor.n == true){
			mouseChangeRight.SetActive(false);
			mouseChangeLeft.SetActive(true);
		}
		else if (s_cursor.v == false && s_cursor.m == true){
			mouseChangeRight.SetActive(false);
			mouseChangeDown.SetActive(true);
		}
		else if (s_cursor.v == false && s_cursor.c == true){
			mouseChangeRight.SetActive(false);
		}
	}
	
	void OnTriggerEnter(Collider other) {
		if(other.gameObject.tag == "Player"){
			s_controller.actionOnRight = true;
			s_controller.actionOnUp = false;
			s_controller.actionOnLeft = false;
			s_controller.actionOnDown = false;
		}
	}
}