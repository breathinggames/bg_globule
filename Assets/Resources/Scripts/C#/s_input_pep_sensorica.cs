﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;

public class s_input_pep_sensorica : MonoBehaviour {

	/// also see http://www.alanzucconi.com/2015/10/07/how-to-integrate-arduino-with-unity/
	/// open serial Mac = /dev/tty.usbmodem621, PC = COM3 or COM4, for higher PC ports = "\\\\.\\COM25"; baud = 9600
	SerialPort stream = new SerialPort("/dev/cu.usbmodem621", 115200);

	public TextMesh t_text;
	private float f_pressure_inter;

	void Start () {
	}

	void Update () {
		stream.Open();

		// timeout if no data received, in miliseconds
		stream.ReadTimeout = 1;
		if (stream.IsOpen) { // stream.available() > 0; ?
			try {
				// reads serial port
				string s_pressure = stream.ReadLine();

				f_pressure_inter = float.Parse(s_pressure);
				f_pressure_inter = f_pressure_inter / 240;
				s_input.f_pressure = f_pressure_inter;

				print (f_pressure_inter);
			}
			catch (System.Exception) {	

			}
		}	
		stream.Close();
	}
}