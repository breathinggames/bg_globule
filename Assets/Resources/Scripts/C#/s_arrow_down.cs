﻿using UnityEngine;
using System.Collections;

public class s_arrow_down : MonoBehaviour {

	public GameObject camPointer;
	public GameObject mouseChangeDown;
	public GameObject mouseChangeUp;
	public GameObject mouseChangeLeft;
	public GameObject mouseChangeRight;

	void Start() {
	}

	void Update() {
	}

	void OnMouseDown() {
		if(s_cursor.m == true){
			GetComponent<AudioSource>().Play();
			mouseChangeDown.SetActive(true);
			s_input_time.arrow_down -= 1;
		}
		else if (s_cursor.m == false && s_cursor.b == true){
			GetComponent<AudioSource>().Play();
			mouseChangeDown.SetActive(false);
			mouseChangeUp.SetActive(true);
		}
		else if (s_cursor.m == false && s_cursor.n == true){
			GetComponent<AudioSource>().Play();
			mouseChangeDown.SetActive(false);
			mouseChangeLeft.SetActive(true);
		}
		else if (s_cursor.m == false && s_cursor.v == true){
			GetComponent<AudioSource>().Play();
			mouseChangeDown.SetActive(false);
			mouseChangeRight.SetActive(true);
		}
		else if (s_cursor.m == false && s_cursor.c == true){
			GetComponent<AudioSource>().Play();
			mouseChangeDown.SetActive(false);
		}
	}
	
	void OnTriggerEnter(Collider other) {
		if(other.gameObject.tag == "Player"){
			s_controller.actionOnRight = false;
			s_controller.actionOnUp = false;
			s_controller.actionOnLeft = false;
			s_controller.actionOnDown = true;
		}
	}
}