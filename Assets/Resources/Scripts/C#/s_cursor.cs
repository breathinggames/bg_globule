﻿using UnityEngine;
using System.Collections;

public class s_cursor : MonoBehaviour {

	static public bool v = false;
	static public bool b = false;
	static public bool n = false;
	static public bool m = false;
	static public bool c = false;
	public Texture2D cursorTexture01;
	public Texture2D cursorTexture02;
	public Texture2D cursorTexture03;
	public Texture2D cursorTexture04;
	private CursorMode cursorMode = CursorMode.Auto;
	private Vector2 hotSpot = Vector2.zero;
	
	void Start() {
	}

	void Update () {
		if (Input.GetKeyDown(KeyCode.RightArrow) && (s_input_time.arrow_right > 1))
		{
			Cursor.SetCursor(cursorTexture01, hotSpot, cursorMode);
			v = true;
			b = false;
			n = false;
			m = false;
			c = false;
		}
		if (Input.GetKeyDown(KeyCode.UpArrow) && (s_input_time.arrow_up > 1))
		{
			Cursor.SetCursor(cursorTexture02, hotSpot, cursorMode);
			b = true;
			v = false;
			n = false;
			m = false;
			c = false;
		}
		if (Input.GetKeyDown(KeyCode.LeftArrow) && (s_input_time.arrow_left > 1))
		{
			Cursor.SetCursor(cursorTexture03, hotSpot, cursorMode);
			n = true;
			v = false;
			b = false;
			m = false;
			c = false;
		}
		if (Input.GetKeyDown(KeyCode.DownArrow) && (s_input_time.arrow_down > 1))
		{
			Cursor.SetCursor(cursorTexture04, hotSpot, cursorMode);
			m = true;
			v = false;
			b = false;
			n = false;
			c = false;
		}
		if (Input.GetKeyDown(KeyCode.A))
		{
			Cursor.SetCursor(null, hotSpot, cursorMode);
			c = true;
			m = false;
			v = false;
			b = false;
			n = false;
		}
	}
}