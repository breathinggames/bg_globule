﻿using UnityEngine;
using System.Collections;

public class s_square_control : MonoBehaviour {

	public GameObject cube;
	public Material matGo;
	public Material matChange;
	public Material m_jungle_gone;
	public Material m_jungle_square;
	public Material m_matrix_gone;
	public Material m_matrix_square;
	public Material m_savane_gone;
	public Material m_savane_square;

	private bool noGo = false;

	void Start () {
	//	cube.renderer.material = matGo;	
		if (s_avatar.s_style == "matrix")
			cube.GetComponent<Renderer>().material = m_matrix_square;
		else if (s_avatar.s_style == "savane")
			cube.GetComponent<Renderer>().material = m_savane_square;
		else
			cube.GetComponent<Renderer>().material = m_jungle_square;
	}

	void Update () {
	/*	if (Input.GetKeyUp(KeyCode.Space)) {
			print ("b");
			globule.GetComponent(BallControl).enabled = true;
			transform.Translate(2 * Time.deltaTime,0,0);
		}	*/
	}
		
	void OnTriggerEnter(Collider other){
		if(other.gameObject.tag == "Player"){
			//	other.gameObject.GetComponent(BallControl).enabled = false;
			
			if(noGo){
				s_controller.actionOnRight = false;
				s_controller.actionOnUp = false;
				s_controller.actionOnLeft = false;
				s_controller.actionOnDown = false;
				Cursor.visible = true;
				Screen.lockCursor = false;
				if(Application.loadedLevelName == "3_path"){
					Application.LoadLevel(1);	
				}
				else if (Application.loadedLevelName == "4_path"){
					Application.LoadLevel(1);	
				}
			}
		}
	}
	
	void OnTriggerExit(Collider other){
		if(other.gameObject.tag == "Player"){
		//	cube.renderer.material = matChange;
			if (s_avatar.s_style == "matrix")
				cube.GetComponent<Renderer>().material = m_matrix_gone;
			else if (s_avatar.s_style == "savane")
				cube.GetComponent<Renderer>().material = m_savane_gone;
			else
				cube.GetComponent<Renderer>().material = m_jungle_gone;
			noGo = true;
		}
	}
}