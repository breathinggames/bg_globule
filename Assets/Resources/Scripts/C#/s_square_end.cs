﻿using UnityEngine;
using System.Collections;

public class s_square_end : MonoBehaviour {

	public GameObject party;
	public GameObject trigCheck01;
	public GameObject trigCheck02;
	public GameObject next2;
	public GameObject cube;
	public Material matNoGo;
	public Material matChange;
	private CursorMode cursorMode = CursorMode.Auto;
	private Vector2 hotSpot = Vector2.zero;

	public Material m_jungle_goal;
	public Material m_matrix_goal;
	public Material m_savane_goal;

	void Start () {
	//	cube.renderer.material = matNoGo;
		if (s_avatar.s_style == "matrix")
			cube.GetComponent<Renderer>().material = m_matrix_goal;
		else if (s_avatar.s_style == "savane")
			cube.GetComponent<Renderer>().material = m_savane_goal;
		else
			cube.GetComponent<Renderer>().material = m_jungle_goal;
	}
	
	// Update is called once per frame
	void Update () {
		if(s_end_trigger1.trig01 == true && s_end_trigger2.trig02 == true) {
		//	cube.renderer.material = matChange;
			if (s_avatar.s_style == "matrix")
				cube.GetComponent<Renderer>().material = m_matrix_goal;
			else if (s_avatar.s_style == "savane")
				cube.GetComponent<Renderer>().material = m_savane_goal;
			else
				cube.GetComponent<Renderer>().material = m_jungle_goal;
		}
	}
	
	void OnTriggerEnter(Collider other){
		if(other.gameObject.tag == "Player" && s_end_trigger1.trig01 == true && s_end_trigger2.trig02 == true){
			other.gameObject.GetComponent<s_controller>().enabled = false;
			party.SetActive(true);
			
			if(next2)
				next2.SetActive(true);
			
			Cursor.SetCursor(null, hotSpot, cursorMode);
			Cursor.visible = true;
			Screen.lockCursor = false;
		}
	}
}