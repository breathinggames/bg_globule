﻿using UnityEngine;
using System.Collections;

public class s_arrow_up : MonoBehaviour {

	public GameObject camPointer;
	public GameObject mouseChangeDown;
	public GameObject mouseChangeUp;
	public GameObject mouseChangeLeft;
	public GameObject mouseChangeRight;
	
	void Start() {
	}
	
	void Update() {
	}

	void OnMouseDown() {
		if(s_cursor.b == true){
			mouseChangeUp.SetActive(true);
			s_input_time.arrow_up -= 1;
		}
		else if (s_cursor.b == false && s_cursor.m == true){
			mouseChangeUp.SetActive(false);
			mouseChangeDown.SetActive(true);
		}
		else if (s_cursor.b == false && s_cursor.n == true){
			mouseChangeUp.SetActive(false);
			mouseChangeLeft.SetActive(true);
		}
		else if (s_cursor.b == false && s_cursor.v == true){
			mouseChangeUp.SetActive(false);
			mouseChangeRight.SetActive(true);
		}
		else if (s_cursor.b == false && s_cursor.c == true){
			mouseChangeUp.SetActive(false);
		}
	}
	
	void OnTriggerEnter(Collider other) {
		if(other.gameObject.tag == "Player"){
			s_controller.actionOnRight = false;
			s_controller.actionOnUp = true;
			s_controller.actionOnLeft = false;
			s_controller.actionOnDown = false;
		}
	}
}