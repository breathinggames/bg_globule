﻿using UnityEngine;
using System.Collections;

public class s_avatar : MonoBehaviour {

	static public string s_style = "jungle";
	public GameObject background;
	public GameObject collect;
	public GameObject collected;
	public GameObject goal;
	public GameObject gone;
	public GameObject square;

	public Material m_jungle_protagonist;
	public Material m_jungle_background;
	public Material m_jungle_collect;
	public Material m_jungle_collected;

	public Material m_matrix_protagonist;
	public Material m_matrix_background;
	public Material m_matrix_collect;
	public Material m_matrix_collected;

	public Material m_savane_protagonist;
	public Material m_savane_background;
	public Material m_savane_collect;
	public Material m_savane_collected;


	void Start() {
	}

	void Update () {
		if (Input.GetKey (KeyCode.Alpha1)) {
			GetComponent<Renderer>().material = m_jungle_protagonist;
			background.GetComponent<Renderer>().material = m_jungle_background;
			s_style = "jungle";
		}
		if (Input.GetKey (KeyCode.Alpha2)) {
			GetComponent<Renderer>().material = m_matrix_protagonist;
			background.GetComponent<Renderer>().material = m_matrix_background;
			s_style = "matrix";
		}
		if (Input.GetKey (KeyCode.Alpha3)) {
			GetComponent<Renderer>().material = m_savane_protagonist;
			background.GetComponent<Renderer>().material = m_savane_background;
			s_style = "savane";
		}
	}
}
