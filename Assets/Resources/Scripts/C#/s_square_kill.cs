﻿using UnityEngine;
using System.Collections;

public class s_square_kill : MonoBehaviour {

	void Start() {
	}
	
	void Update() {
	}

	void OnTriggerEnter(Collider other){
		if(other.tag == "killBox"){
			s_controller.actionOnRight = false;
			s_controller.actionOnUp = false;
			s_controller.actionOnLeft = false;
			s_controller.actionOnDown = false;
			Cursor.visible = true;
			Screen.lockCursor = false;

			if(Application.loadedLevelName == "niveauTestCube02")
				Application.LoadLevel(2);	
			else if (Application.loadedLevelName == "niveauTestCube03")
				Application.LoadLevel(3);
		}
	}
}