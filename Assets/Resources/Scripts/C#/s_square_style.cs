﻿using UnityEngine;
using System.Collections;

public class s_square_style : MonoBehaviour {
	
	public Material m_jungle;
	public Material m_matrix;
	public Material m_savane;

	void Start () {
	}

	void Update () {
		if (s_avatar.s_style == "matrix")
			GetComponent<Renderer>().material = m_matrix;
		else if (s_avatar.s_style == "savane")
			GetComponent<Renderer>().material = m_savane;
		else
			GetComponent<Renderer>().material = m_jungle;
	}
}
