﻿using UnityEngine;
using System.Collections;

public class s_square_collect : MonoBehaviour {

	public GameObject cube;
	public Material matGo;
	public Material matChange;
	private bool noGo = false;


	void Start () {
		cube.GetComponent<Renderer>().material = matGo;	
	}

	void Update() {
	}

	void OnTriggerEnter(Collider other){
		if(other.gameObject.tag == "Player"){
			if(noGo){
				s_controller.actionOnRight = false;
				s_controller.actionOnUp = false;
				s_controller.actionOnLeft = false;
				s_controller.actionOnDown = false;
				if(Application.loadedLevelName == "niveauTestCube02"){
					Application.LoadLevel(2);	
				}
				else if (Application.loadedLevelName == "niveauTestCube03"){
					Application.LoadLevel(3);	
				}
			}
		}
	}
	
	void OnTriggerExit(Collider other){
		if(other.gameObject.tag == "Player"){
			cube.GetComponent<Renderer>().material = matChange;
			noGo = true;
		}
	}
}