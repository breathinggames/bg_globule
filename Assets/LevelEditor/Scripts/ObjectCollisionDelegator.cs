﻿using UnityEngine;
using System;
using System.Collections;

public class ObjectCollisionDelegator : MonoBehaviour {

	public Action<Collision> OnEnterDelegate;
	public Action<Collision> OnExitDelegate;

	void OnCollisionEnter(Collision collision)
	{
		if (OnEnterDelegate == null) return;

		OnEnterDelegate(collision);
	}
	
	void OnCollisionExit(Collision collision)
	{
		if (OnExitDelegate == null) return;

		OnExitDelegate(collision);
	}
}
