﻿using UnityEngine;
using System.Collections;
using System.Text;
using System;
using System.Linq;

public class Console : MonoBehaviour {
	public KeyCode keyToggle = KeyCode.C;
	public bool visible;

	Vector2 scrollPosition = Vector2.zero;
	int loggerLine;
	int lastLoggerLine;
	static Rect clientRect = new Rect(100, 100, 800, 300);
	static Rect scrollViewPosition = new Rect(3, 20, clientRect.width - 6, clientRect.height - 23);
	StringBuilder sb = new StringBuilder();

	void OnEnable()
	{
		Application.RegisterLogCallback(LogCallback);
	}

	void OnDisable()
	{
		Application.RegisterLogCallback(null);
	}

	void Update()
	{
		if (Input.GetKeyDown(keyToggle))
			visible = !visible;
	}

	void OnGUI()
	{
		if (visible)
			clientRect = GUI.Window(1, clientRect, WindowCallback, "Console");
	}

	void LogCallback(string logString, string stackTrace, LogType type)
	{
		var msg = string.Format("{0}: {1}{2}{3}{2}", type.ToString(), logString, Environment.NewLine, stackTrace);
		sb.Append(msg);
		loggerLine += msg.Count(s => s == '\n');
	}

	void WindowCallback(int id)
	{
		var height = GUI.skin.label.padding.vertical + (loggerLine * GUI.skin.label.lineHeight);
		var labelPosition = new Rect(0, 0, scrollViewPosition.width - 20, height);
		if (loggerLine != lastLoggerLine)
		{
			scrollPosition.y = height - scrollViewPosition.height;
		}
		lastLoggerLine = loggerLine;

		scrollPosition = GUI.BeginScrollView(scrollViewPosition, scrollPosition, labelPosition);
		GUI.Label(labelPosition, sb.ToString());
		GUI.EndScrollView();

		GUI.DragWindow();
	}
}
