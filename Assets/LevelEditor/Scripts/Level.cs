using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using System.IO;
using System.Linq;
using System;
using System.Collections;
using Procurios.Public;
using System.Security.Cryptography;

public class Level
{
	private GameObject boardPieceTemplate;
	private Vector3 scale;
	private Color removeColor;
	private Material[] materials;

	public string Name { get; set; }
	public List<BoardPiece> BoardPieces { get; private set; }

	public Level(GameObject boardPieceTemplate, float spacing, Color removeColor, Material[] materials)
	{
		this.BoardPieces = new List<BoardPiece>();
		this.boardPieceTemplate = boardPieceTemplate;
		this.scale = new Vector3(
			boardPieceTemplate.GetComponent<Renderer>().bounds.size.x + spacing,
			boardPieceTemplate.GetComponent<Renderer>().bounds.size.y + spacing,
			boardPieceTemplate.GetComponent<Renderer>().bounds.size.z + spacing
		);
		this.removeColor = removeColor;
		this.materials = materials;
	}

	public BoardPiece Add(Vector3 point)
	{
		var boardPiece = new BoardPiece(
			boardPieceTemplate, 
			point,
			scale,
			removeColor
		);

		BoardPieces.Add(boardPiece);

		return boardPiece;
	}

	private void Add(Vector3 point, int? textureIndex)
	{
		var boardPiece = Add(point);
		
		if (textureIndex.HasValue) {
			boardPiece.PlaneMaterial = materials[textureIndex.Value];
		}
	}

	public BoardPiece GetBoardPiece(GameObject gameObject)
	{
		return BoardPieces.Single(bp => bp.IsEqual(gameObject));
	}

	public void Remove(BoardPiece boardPiece)
	{
		BoardPieces.Remove(boardPiece);
		boardPiece.Destroy();
	}

	public void Clear()
	{
		foreach(var boardPiece in BoardPieces)
		{
			boardPiece.Destroy();
		}
		BoardPieces.Clear();
	}

	public string SaveToJson()
	{
		var jsonData = new Hashtable();
		jsonData["name"] = this.Name;
		var cubes = new ArrayList();
		foreach(var bp in BoardPieces) {
			var cube = new Hashtable();
			cube["x"] = bp.PosX;
			cube["y"] = bp.PosY;
			cube["z"] = bp.PosZ;
			var iEnumerable = materials.Select(s => s.mainTexture.name);
			var stringArray = iEnumerable.ToArray();
			if (bp.PlaneMaterial != null && bp.PlaneMaterial.mainTexture != null) {
				var planeMainTextureName = bp.PlaneMaterial.mainTexture.name;
				var textureIndex = Array.IndexOf(stringArray, planeMainTextureName);
				cube["texture"] = textureIndex;
			}
			cubes.Add(cube);
		}
		jsonData["cubes"] = cubes;
		
		return JSON.JsonEncode(jsonData);
	}

	public string SaveToXml()
	{
		var sb = new StringBuilder();
		var xmlWriter = XmlWriter.Create(sb);

		xmlWriter.WriteProcessingInstruction("xml", "version='1.0' encoding='utf-8'");

		xmlWriter.WriteStartElement("level");
		xmlWriter.WriteAttributeString("name", Name);

		foreach(var bp in BoardPieces)
		{
			xmlWriter.WriteStartElement("cube");
			xmlWriter.WriteAttributeString("x", bp.PosX.ToString());
			xmlWriter.WriteAttributeString("y", bp.PosY.ToString());
			xmlWriter.WriteAttributeString("z", bp.PosZ.ToString());

			var iEnumerable = materials.Select(s => s.mainTexture.name);
			var stringArray = iEnumerable.ToArray();
			if (bp.PlaneMaterial != null && bp.PlaneMaterial.mainTexture != null) {
				var planeMainTextureName = bp.PlaneMaterial.mainTexture.name;
				var textureIndex = Array.IndexOf(stringArray, planeMainTextureName);
				xmlWriter.WriteAttributeString("texture", textureIndex.ToString());
			}

			xmlWriter.WriteEndElement();
		}

		xmlWriter.WriteEndElement();

		xmlWriter.Close();

		return sb.ToString();
	}
	
	public void LoadFromXml(string xml)
	{
		this.Clear();

		try {
			var stringReader = new StringReader(xml);
			var xmlReader = XmlReader.Create(stringReader);

			while (!xmlReader.EOF)
			{
				xmlReader.Read();

				switch(xmlReader.Name)
				{
				case "level":
					xmlReader.GetAttribute("name");
					break;
				case "cube":
					var x = float.Parse(xmlReader.GetAttribute("x"));
					var y = float.Parse(xmlReader.GetAttribute("y"));
					var z = float.Parse(xmlReader.GetAttribute("z"));
					var texture = xmlReader.GetAttribute("texture");
					var textureIndex = string.IsNullOrEmpty(texture)
						? (int?)null
						: Int32.Parse(xmlReader.GetAttribute("texture"));
					var point = new Vector3(x, y, z);
					Add(point, textureIndex);
					break;
				}
			}

			xmlReader.Close();
			stringReader.Close();
		} catch (Exception e) {
			Debug.Log(e);
			this.Clear();
		}
	}

	public void LoadFromJson(string json)
	{
		this.Clear();

		try {
			var levels = JSON.JsonDecode(json) as Hashtable;
			this.Name = levels["name"] as String;
			var cubes = levels["cubes"] as ArrayList;
			foreach(Hashtable cube in cubes) {
				var x = float.Parse(cube["x"].ToString());
				var y = float.Parse(cube["y"].ToString());
				var z = float.Parse(cube["z"].ToString());
				var point = new Vector3(x, y, z);
				var texture = cube["texture"] as Int32?;
				this.Add(point, texture);
			}
		} catch (Exception e) {
			Debug.Log(e);
			this.Clear();
		}
	}
}
