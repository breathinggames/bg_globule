using UnityEngine;
using System;

public class Ghost : BoardPiece
{
	private static Ghost instance;
	private bool collisionDetectionAdded;
	private Rect viewportRect;

	public bool Visible { get; set; }
	public bool Enabled { get; private set; }

	public static Ghost GetInstance(GameObject cube, Vector3 point, Vector3 scale, Color color)
	{
		if (instance == null)
		{
			instance = new Ghost(cube, point, scale, color);
		}
		return instance;
	}

	private Ghost(GameObject cube, Vector3 point, Vector3 scale, Color color)
		: base(cube, point, scale, color)
	{
		Visible = true;

		// TODO viewport stuff should not be here... static class?
		var topLeft = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
		var bottomRight = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 1));
		viewportRect = new Rect(
			(Mathf.Round(topLeft.x / scale.x) + 0.5f) * scale.x,
			(Mathf.Round(topLeft.z / scale.z) + 1.5f) * scale.z,
			(Mathf.Round((bottomRight.x - topLeft.x) / scale.x) - 1.0f) * scale.x,
			(Mathf.Round((bottomRight.z - topLeft.z) / scale.z) - 2.0f) * scale.z
		);
	}

	public void Update(Vector3 point)
	{
		if (!viewportRect.Contains(new Vector2(point.x, point.z)))
		{
			Enabled = false;
			gameObject.GetComponent<Renderer>().enabled = false;
			return;
		}

		Enabled = true;
		gameObject.GetComponent<Renderer>().enabled = Visible;
		
		gameObject.transform.position = new Vector3(
			Mathf.Round(point.x / scale.x) * scale.x, 
			point.y + (scale.y / 2), 
			Mathf.Round(point.z / scale.z) * scale.z
		);
	}

	public void AddCollisionDetection(Action<Collision> onEnterDelegate, Action<Collision> onExitDelegate)
	{
		if (collisionDetectionAdded) return;

		var ocd = gameObject.AddComponent<ObjectCollisionDelegator>();
		ocd.OnEnterDelegate = onEnterDelegate;
		ocd.OnExitDelegate = onExitDelegate;

		collisionDetectionAdded = true;
	}
}
