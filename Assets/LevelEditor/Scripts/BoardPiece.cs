﻿using UnityEngine;
using System.Collections;
using System.Xml;

public class BoardPiece
{
	private GameObject cube;
	protected GameObject gameObject;
	private GameObject plane;
	private static int cpt = 1;
	private bool _isColliding;
	private Color color;
	protected Vector3 scale;

	public float PosX { get { return gameObject.transform.position.x; } }
	public float PosY { get { return gameObject.transform.position.y; } }
	public float PosZ { get { return gameObject.transform.position.z; } }

	public bool IsColliding
	{
		get {
			return _isColliding;
		}
		set {
			_isColliding = value;
			gameObject.GetComponent<Renderer>().material.CopyPropertiesFromMaterial(cube.GetComponent<Renderer>().sharedMaterial);
			if (_isColliding) {
				gameObject.GetComponent<Renderer>().material.color = color;
			}
		}
	}

	public BoardPiece(GameObject cube, Vector3 point, Vector3 scale, Color color)
	{
		this.cube = cube;
		this.color = color;
		this.scale = scale;
		Create(cube, point, scale);
	}

	public void Destroy()
	{
		Object.Destroy(gameObject);
		gameObject = null;
	}

	private void Create(GameObject cube, Vector3 point, Vector3 scale)
	{
		gameObject = (GameObject)Object.Instantiate(
			cube, 
			new Vector3(
				Mathf.Round(point.x / scale.x) * scale.x, 
				point.y + (scale.y / 2), 
				Mathf.Round(point.z / scale.z) * scale.z
			),
			Quaternion.identity
		);

		plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
		plane.GetComponent<Collider>().enabled = false;
		plane.transform.parent = gameObject.transform;
		plane.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
		plane.transform.localPosition = new Vector3(0, 1, 0);
		plane.transform.localRotation = Quaternion.Euler(new Vector3(0, 180, 0));
		plane.SetActive(false);

		gameObject.name = gameObject.name.Replace("(Clone)", cpt.ToString());
		cpt++;
	}

	public bool IsEqual(GameObject gameObject)
	{
		return gameObject == this.gameObject;
	}

	public Material PlaneMaterial
	{
		get {
			return plane.GetComponent<Renderer>().material;
		}
		set {
			if (plane.activeSelf && value == null) {
				plane.SetActive(false);
			} else if (!plane.activeSelf) {
				plane.SetActive(true);
			}

			plane.GetComponent<Renderer>().material = value;
		}
	}
}
