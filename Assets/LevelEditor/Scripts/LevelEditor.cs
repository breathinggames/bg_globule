﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Security.Cryptography;

public class LevelEditor : MonoBehaviour
{
	public GameObject BoardObject;
	public string TerrainCollisionTag = "Floor";
	public float Spacing = 0.01f;
	public Color RemoveColor = new Color(1f, 0f, 0f, 0.6f);
	public Color AddColor = new Color(0f, 1f, 0f, 0.6f);

	public Texture2D[] Cursors;
	public Vector2 CursorHotSpot;
	public CursorMode CursorMode;
	public int CursorIndex;

	public Material[] Materials;
	public int[] Limits;
	public KeyCode BoardObjectKey;
	public KeyCode[] PlaneKeys;

	public string Url;
	public string Username;
	public string Key;

	private Ghost ghost;

	private bool isColliding;
	private BoardPiece collidingGameObject;
	private Level level;

	private Vector3 scale;

	private BoardPiece lastSelectedPiece;

	private string sessionId;

	private int[] usedLimits;

	private bool showLoadWindow;

	private bool showSaveWindow;

	private IDictionary<int, IList<string>> levels;

	private Vector2 loadWindowScrollPosition = Vector2.zero;
	static Rect clientRect = new Rect(100, 100, 300, 300);
	static Rect scrollViewPosition = new Rect(3, 20, clientRect.width - 6, clientRect.height - 23);

	// Use this for initialization
	void Start()
	{
		usedLimits = new int[Limits.Length];

		scale = new Vector3(
			BoardObject.GetComponent<Renderer>().bounds.size.x + Spacing,
			BoardObject.GetComponent<Renderer>().bounds.size.y + Spacing,
			BoardObject.GetComponent<Renderer>().bounds.size.z + Spacing
		);

		level = new Level(BoardObject, Spacing, RemoveColor, Materials);

		CreateGhost();
	}

	// Update is called once per frame
	void Update()
	{
		UpdateUserAction();

		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit[] hits = Physics.RaycastAll(ray, Mathf.Infinity);

		foreach(RaycastHit hit in hits)
		{
			if (hit.collider.tag == TerrainCollisionTag)
			{
				UpdateEditor(hit);
			}
		}
	}

	void OnGUI()
	{
		if (GUI.Button(new Rect(20, Screen.height - 70, 150, 25), "Sauvegarder")) {
			showSaveWindow = true;
			ghost.Visible = false;
			StartCoroutine(Util.GetFromServer(
				Url + "?name=" + WWW.EscapeURL(Username),
			    OnLevelsLoadedSuccess,
			    OnLevelsLoadedError
			));
		} else if (GUI.Button(new Rect(20, Screen.height - 45, 150, 25), "Ouvrir")) {
			showLoadWindow = true;
			ghost.Visible = false;
			StartCoroutine(Util.GetFromServer(
				Url + "?name=" + WWW.EscapeURL(Username),
				OnLevelsLoadedSuccess,
				OnLevelsLoadedError
			));
		}
		
		if (showLoadWindow) {
			GUI.ModalWindow(1, clientRect, LoadWindowFunc, "Ouvrir");
		} else if (showSaveWindow) {
			GUI.ModalWindow(2, clientRect, LoadWindowFunc, "Sauvegarder");
		}

		var guiStyle = new GUIStyle(GUI.skin.button);
		guiStyle.imagePosition = ImagePosition.ImageAbove;

		var boardObjectButtonPressed = GUI.Button(
			new Rect(190, Screen.height - 70, 50, 50),
			new GUIContent(BoardObjectKey.ToString(), BoardObject.GetComponent<Renderer>().sharedMaterial.mainTexture),
			guiStyle
		);
		if (Input.GetKeyDown(BoardObjectKey) || boardObjectButtonPressed) {
			Cursor.SetCursor(null, CursorHotSpot, CursorMode);
			CursorIndex = 0;
			ghost.Visible = true;
		}

		for(var i = 0; i < Cursors.Length; i++) {
			var key = PlaneKeys[i];
			var str = key.ToString(true);
			var buttonPressed = GUI.Button(
				new Rect(260 + (i * 70), Screen.height - 70, 50, 50),
				new GUIContent(str, Cursors [i]),
				guiStyle
			);

			if (Input.GetKeyDown(key) || buttonPressed) {
				Cursor.SetCursor(Cursors[i], CursorHotSpot, CursorMode);
				CursorIndex = i + 1;
				ghost.Visible = false;
			}
		}
	}

	public void SetUsername(string username)
	{
		Username = username;
	}

	void LoadWindowFunc(int id)
	{
		if (this.levels != null) {
			var height = 30 * levels.Count;
			var viewRect = new Rect(0, 0, scrollViewPosition.width - 20, height);
			loadWindowScrollPosition = GUI.BeginScrollView(scrollViewPosition, loadWindowScrollPosition, viewRect);

			var i = 0;
			foreach(var keyValuePair in this.levels) {
				if (GUI.Button(new Rect(5, 5 + (i * 25), viewRect.width - 10, 25), keyValuePair.Value[0])) {
					if (id == 1) {
						// Load
						this.level.LoadFromJson(keyValuePair.Value[1]);
						this.showLoadWindow = false;
						this.ghost.Visible = true;
					} else {
						// Save
						var json = this.level.SaveToJson();
						var md5 = MD5CryptoServiceProvider.Create();
						// TODO comment on connait le id?
						var bytes = System.Text.Encoding.UTF8.GetBytes("id" + Username + json + "ubc42");
						var hash = md5.ComputeHash(bytes);
						Util.GetFromServer(
							Url + "?name=" + WWW.EscapeURL(Username) + "&code=" + json + "&hash=" + hash,
							OnLevelSaveSuccess,
							OnLevelsLoadedError
						);
					}
				}
			}

			GUI.EndScrollView();
		}
	}

	void OnLevelsLoadedSuccess(string content)
	{
		// TODO séparer les enregistrements avec des changements de ligne
		this.levels = new Dictionary<int, IList<string>>();
		var lines = content.Split(new char[] { '\r', '\n' });
		foreach(var line in lines) {
			var parts = line.Split('|');
			var id = Int32.Parse(parts[0]);
			this.levels.Add(id, new List<string>(parts.Skip(1).Take(3)));
		}
	}

	void OnLevelSaveSuccess(string content)
	{
		this.showLoadWindow = false;
		this.ghost.Visible = true;
	}
	
	void OnLevelsLoadedError(string error)
	{
		Debug.Log(error);
		this.showLoadWindow = false;
		this.ghost.Visible = true;
	}

	void CreateGhost()
	{
		ghost = Ghost.GetInstance(
			BoardObject,
			new Vector3(300, Spacing, 300),
			scale,
			AddColor
		);

		ghost.AddCollisionDetection(
			delegate(Collision collision) {
				if (collision.collider.gameObject.tag != TerrainCollisionTag)
				{
					ghost.Visible = false;
					isColliding = true;
					collidingGameObject = level.GetBoardPiece(collision.gameObject);
				}
			},
			delegate(Collision collision) {
				if (CursorIndex == 0)
					ghost.Visible = true;
				isColliding = false;
			}
		);
	}

	void UpdateEditor(RaycastHit hit)
	{
		UpdateCurrentPiece(hit);

		UpdateEditorAction(hit.point);

		ghost.Update(hit.point);
	}

	void UpdateEditorAction(Vector3 point)
	{
		if (Input.GetMouseButtonDown(0) && ghost.Enabled)
		{
			if (CursorIndex == 0)
			{
				if (!isColliding && ghost.Visible)
				{
					level.Add(point);
				}
				else if (isColliding)
				{
					SubstractFromUsedLimits();

					level.Remove(lastSelectedPiece);
					lastSelectedPiece = null;
					isColliding = false;
					ghost.Visible = true;
				}
			}
			else if (isColliding)
			{
				var i = CursorIndex - 1;
				if (Limits[i] > 0 && usedLimits[i] >= Limits[i]) return;

				SubstractFromUsedLimits();

				lastSelectedPiece.PlaneMaterial = Materials[i];
				usedLimits[i]++;
			}
		}
	}

	void SubstractFromUsedLimits()
	{
		var planeTexture = lastSelectedPiece.PlaneMaterial.mainTexture;
		var index = Array.IndexOf(Materials.Select(m => m.mainTexture).ToArray(), planeTexture);
		if (index < 0) return;
		usedLimits[index]--;
	}

	void UpdateUserAction()
	{
		if (Input.GetMouseButton(1))
		{
			Camera.main.transform.Translate(-Input.GetAxis("Mouse X"), -Input.GetAxis("Mouse Y"), 0);
		}
		else
		{
			var mouseScroll = Input.GetAxis("Mouse ScrollWheel");
			if (mouseScroll != 0)
			{
				Camera.main.orthographicSize -= mouseScroll;
			}
		}
	}

	void UpdateCurrentPiece(RaycastHit hit)
	{
		if (lastSelectedPiece != null)
		{
			lastSelectedPiece.IsColliding = false;
			lastSelectedPiece = null;
		}

		if (!isColliding) return;

		if (CursorIndex == 0)
			collidingGameObject.IsColliding = true;

		lastSelectedPiece = collidingGameObject;
	}
}

