using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Security.Cryptography;

public static class KeyCodeExtensions
{
	private static IList<string> keywords = new List<string> {
		"Alpha",
		"Arrow"
	};

	private static IDictionary<string, string> translations = new Dictionary<string, string> {
		{"Up", "Haut"},
		{"Right", "Droite"},
		{"Down", "Bas"},
		{"Left", "Gauche"}
	};

	public static string ToString(this KeyCode keyCode, bool removeKeyword)
	{
		if (!removeKeyword) return keyCode.ToString();

		var str = keyCode.ToString();

		// Remove keywords
		foreach(var keyword in keywords) {
			str = str.Replace(keyword, "");
		}

		// Translate
		foreach(var keyValuePair in translations) {
			str = str.Replace(keyValuePair.Key, keyValuePair.Value);
		}

		return str;
	}
}